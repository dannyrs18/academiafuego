@extends('template/base')

@section('content')
    <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
            <!-- START sidebar-a block -->
            <aside class="tm-sidebar-a uk-width-large-1-4 uk-width-small-1-1">
                <div class="uk-panel uk-panel-header uk-panel-box">
                    <h3 class="uk-panel-title"><i class="uk-icon-tag"></i> Misión</h3>
                    <p style="text-align: justify">Nuestra principal misión es impartir clases de baile a niños, niñas, jóvenes y adultos, teniendo 
                    en cuenta el profesionalismo de nuestros instructores y el programa de enseñanza que aplicamos. Brindamos atención personalizada acorde a las condiciones físicas y capacidades de cada uno de nuestros 
                    alumnos.</p>
                </div>
                <div class="uk-panel uk-panel-header uk-panel-box">
                    <h3 class="uk-panel-title"><i class="uk-icon-tag"></i> Visión</h3>
                    <p style="text-align: justify">Hacer de “Fuego” la mejor academia de baile reconocida a nivel local y regional, caracterizándose por su excelente servicio al cliente, calidad en el trato humano, profesionalismo, igualdad, transparencia y respeto, que brinde un espacio 
                        donde las personas puedan aprender y/o mejorar su técnica de baile; así mismo diversificar  nuestro programa de actividades dancísticas, de acuerdo a las necesidades de la comunidad y promover la adquisición de hábitos saludables en un 
                        marco de respeto, alegría y cordialidad.</p>
                </div>
            </aside>
            <!-- END sidebar-a block -->
            <div class="tm-main uk-width-large-3-4 uk-width-small-1-1" style="min-height: 2258px;">
                <main class="tm-content uk-position-relative">
                
                    <br>
                    
                    <div id="system-message-container"></div>
                    
                    <!-- START Dummy item block -->
                    <article class="uk-article">
                        <h1 class="uk-article-title">
                            Quienes Somos
                        </h1>
                        <div>
                            <p style="text-align: justify">Somos una Academia De Baile que comienza su recorrido El 23 de Septiembre de Agosto del 2006 con la finalidad de brindarle a la comunidad Lojana un espacio para desarrollar el arte del Baile en varias de sus modalidades.</p>
                            <p style="text-align: justify">Haciendo posible la integración de ritmos tropicales, de salón y modernos, gracias al acompañamiento de todo un equipo de bailarines, dotados con la capacidad y experiencia para transmitir el conocimiento rítmico a todos aquellos 
                                que deseen aprender un ritmo especifico o ser profesionales en el contexto artístico del baile.</p>
                            <p style="text-align: justify">Desde sus inicios, nos hemos caracterizado por ser una Academia organizada en donde se destaca su formación humana y sus capacidades artísticas coreográficas en los diversos ritmos.</p>
                        </div>
                    </article>
                    <article class="uk-article">
                        <h1 class="uk-article-title">
                            Objetivo Principal
                        </h1>
                        <div>
                            <p style="text-align: justify">Valorar el esfuerzo y la dedicación de los alumnos, reconociendo en cada uno su potencial creativo, abandonando toda posibilidad de discriminación de edades, clases sociales o culturales.</p>
                        </div>
                    </article>
                    <article class="uk-article">
                        <h1 class="uk-article-title">
                            Principios
                        </h1>
                        <div>
                            <p style="text-align: justify">Fomentar el arte del baile a través de la Unión de profesionales dispuestos a brindar un servicio con calidad en la Enseñanza a nuestros alumnos con responsabilidad y compromiso para Generar ideas innovadoras que desarrollen su 
                                talento mediante la correcta Organización que caracteriza a la academia de Baile “Fuego” teniendo una variada programación, acertada planeación que genere al alumno seguridad Confianza, cumplimiento y superación de expectativas.</p>
                        </div>
                    </article>
                    <!-- END Dummy item block -->
                    
                </main>
            </div>
            
        </div>
    </div>
@endsection