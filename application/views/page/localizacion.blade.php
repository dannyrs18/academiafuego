@extends('template/base')

@section('content')
<div class="tm-fullscreen uk-position-relative">
    
    <div class="akslider-module ">
        <div class="uk-slidenav-position" data-uk-slideshow="{height: 'auto', animation: 'scale', duration: '500', autoplay: false, autoplayInterval: '7000', videoautoplay: false, videomute: true, kenburns: true}">
            <ul class="uk-slideshow">
                <li class="uk-cover uk-height-viewport uk-active">
                    <div class="uk-cover-background uk-position-cover uk-animation-scale uk-animation-reverse uk-animation-15 uk-animation-middle-left" style="background-image: url(images/demo/visuals/slide-contacts.jpg);"></div>
                    <img src="<?= base_url() ?>assets/images/demo/visuals/slide-contacts.jpg" width="800" height="400" alt="Contacts - Slide">
                    <div class="uk-caption uk-caption-panel uk-animation-fade uk-flex uk-flex-center uk-flex-bottom tm-contacts">
                        <div>
                            <div class="uk-grid">
                                
                                
                                
                                
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    
</div>

<div class="uk-container uk-container-center">
    <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
        <div class="tm-main uk-width-medium-1-1">
            <main class="tm-content uk-position-relative">
                <br>
                
                <div id="system-message-container"></div>
                
                <!-- START Article block -->
                <article class="uk-article">
                    <h1 class="uk-article-title">
                        Localización
                    </h1>
                    <div>
                        <div class="uk-grid">
                            <div class="uk-width-3-5">
                                <p>Curabitur blandit tempus porttitor. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Maecenas sed diam eget risus varius blandit sit amet non magna. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Vestibulum id ligula porta felis euismod semper.</p>
                                <p>Donec ullamcorper nulla non metus auctor fringilla. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Sed posuere consectetur est at lobortis. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec.</p>
                                <div class="uk-grid">
                                    <div class="uk-width-1-3">
                                        <h3 class="uk-margin-top">Jorge Lojan</h3>
                                        <p>Calle Lourdes entre<br>
                                            Juan Josè Peña y <br>
                                            24 de Mayo<br>
                                            T: +593-991364690
                                        </p>
                                    </div>
                                    <div class="uk-width-1-3">
                                        <h3 class="uk-margin-top">Helth Care Center</h3>
                                        <p>32 Oatland Rood<br>
                                            SAN JOSE CS 92926 4601<br>
                                            T: +1-354-895-4592<br>
                                            F: +1-745-442-2708
                                        </p>
                                    </div>
                                    <div class="uk-width-1-3">
                                        <h3 class="uk-margin-top">Personal Trainers</h3>
                                        <p>32 Oatland Rood<br>
                                            SAN JOSE CS 92926 4601<br>
                                            T: +1-354-895-4592<br>
                                            F: +1-745-442-2708
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-2-5">
                                <h3>Contactanos!</h3>
                                <div>
                                    <div id="aiContactSafe_form_2">
                                        <div class="aiContactSafe" id="aiContactSafe_mainbody_2">
                                            <div class="contentpaneopen">
                                                <div id="aiContactSafeForm_2">
                                                    <form action="#" method="post" id="adminForm_2" name="adminForm_2" enctype="multipart/form-data">
                                                        <div id="displayAiContactSafeForm_2">
                                                            <div class="aiContactSafe" id="aiContactSafe_contact_form_2">
                                                                <div class="aiContactSafe_row">    
                                                                    <div class="aiContactSafe_contact_form_field_right"><input type="text" name="aics_name_ph" id="aics_name_ph" class="textbox" placeholder="Nombre" value=""></div>
                                                                </div>
                                                                <div class="aiContactSafe_row">
                                                                    <div class="aiContactSafe_contact_form_field_right"><input type="text" name="aics_email_ph" id="aics_email_ph" class="email" placeholder="Correo" value=""></div>
                                                                </div>
                                                                <div class="aiContactSafe_row">
                                                                    <div class="aiContactSafe_contact_form_field_right"><textarea name="aics_message" id="aics_message" cols="40" rows="10" class="editbox" placeholder="Mensaje"></textarea></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div id="aiContactSafeBtns_2">
                                                            <div id="aiContactSafeButtons_left" style="clear:both; display:block; width:100%; text-align:left;">
                                                                <div id="aiContactSafeSend_2" style="float:left;">
                                                                    <div id="aiContactSafeSend_loading_2" style="float:left; margin:2px;">&nbsp;</div>
                                                                    <input type="submit" id="aiContactSafeSendButton_2" value="Enviar" style="float:left; margin:2px;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- END Article block -->
            </main>
        </div>
    </div>
</div>
@endsection