@extends('template/base')

@section('content')
<div class="uk-container uk-container-center">
    <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
        <div class="tm-main uk-width-medium-1-1">
            <main class="tm-content uk-position-relative">
                <br>
                
                <div id="system-message-container"></div>
                
                
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div class="uk-panel uk-panel-header">
                            <h1 class="tm-title">Cursos</h1>
                        </div>
                    </div>
                </div>
                
                <!-- START Article Blog block -->
                <div class="uk-grid" data-uk-grid-match="" data-uk-grid-margin="">



                    <?php 
                        $data = array();
                        foreach (Cursos::all()->where('estado', '1') as $value) {
                            array_push($data, $value);
                        }
                        $contador=0
                    ?>
                    @if (count($data))
                    <div class="uk-width-medium-1-2">
                        @for ($i = 0; $i < intval(count($data))/2; $i++)
                        <article class="uk-article">
                            <h2 class="uk-article-title">
                                <a href="/curso/Bailoterapia" title="Swimming">{{ $data[$i]['disciplina'] }}</a>
                            </h2>
                            <div>
                            <div class="uk-panel tm-classes">
                                <div class="uk-cover-background uk-position-relative" style="background-image: url('<?= base_url() ?>assets/images/demo/classes/class-6.jpg');">
                                    <img src="<?= base_url() ?>assets/images/demo/classes/class-6.jpg" class="uk-invisible" width="610" height="610" alt="5 Unique Workauts to Improve your Deadlift">
                                </div>
                            </div>
                            <div class="jcomments-links"><a class="readmore-link" href="/curso/{{ $data[$i]['slug'] }}" title="Swimming">Mas información ...</a>  </div>
                        </div>
                    </article>
                    <?php $contador = $i+1 ?>
                    @endfor
                    </div>
                    <div class="uk-width-medium-1-2">
                        @for ($i = $contador; $i < intval(count($data)); $i++)
                        <article class="uk-article">
                            <h2 class="uk-article-title">
                                <a href="/curso/Bailoterapia" title="Swimming">Bailoterapia</a>
                            </h2>
                            <div>
                                <div class="uk-panel tm-classes">
                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('<?= base_url() ?>assets/images/demo/classes/class-6.jpg');">
                                        <img src="<?= base_url() ?>assets/images/demo/classes/class-6.jpg" class="uk-invisible" width="610" height="610" alt="5 Unique Workauts to Improve your Deadlift">
                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom">
                                            <div class="ak-overlay-classes uk-width-1-1">
                                                <div class="tm-radial uk-float-left" data-circle-value="1">
                                                    <div>Duración <span>60 minutes</span></div>
                                                </div>
                                                <div class="tm-radial uk-float-left" data-circle-value="1">
                                                    <div>Costo <span>$80</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="jcomments-links"><a class="readmore-link" href="/curso/Bailoterapia" title="Swimming">Mas información ...</a>  </div>
                            </div>
                        </article>
                        @endfor
                    </div>    
                    @else
                    <div class="uk-width-medium-1-2">
                        <!-- START Article 1 block -->
                        <article class="uk-article">
                            <h2 class="uk-article-title">
                                <a href="/curso/Bailoterapia" title="Swimming">Bailoterapia</a>
                            </h2>
                            <div>
                                <div class="uk-panel tm-classes">
                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('<?= base_url() ?>assets/images/demo/classes/class-6.jpg');">
                                        <img src="<?= base_url() ?>assets/images/demo/classes/class-6.jpg" class="uk-invisible" width="610" height="610" alt="5 Unique Workauts to Improve your Deadlift">
                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom">
                                            <div class="ak-overlay-classes uk-width-1-1">
                                                <div class="tm-radial uk-float-left" data-circle-value="1">
                                                    <div>Duración <span>60 minutes</span></div>
                                                </div>
                                                <div class="tm-radial uk-float-left" data-circle-value="1">
                                                    <div>Costo <span>$80</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="jcomments-links"><a class="readmore-link" href="/curso/Bailoterapia" title="Swimming">Mas información ...</a>  </div>
                            </div>
                        </article>
                        <!-- END Article 1 block -->
                        
                        <!-- START Article 2 block -->
                        <article class="uk-article">
                            <h2 class="uk-article-title">
                                <a href="curso/" title="Swimming">Bailes Tropicales</a>
                            </h2>
                            <div>
                                <div class="uk-panel tm-classes">
                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('<?= base_url() ?>assets/images/demo/classes/class-6.jpg');">
                                        <img src="<?= base_url() ?>assets/images/demo/classes/class-6.jpg" class="uk-invisible" width="610" height="610" alt="5 Unique Workauts to Improve your Deadlift">
                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom">
                                            <div class="ak-overlay-classes uk-width-1-1">
                                                <div class="tm-radial uk-float-left" data-circle-value="1">
                                                    <div>Duración <span>60 minutes</span></div>
                                                </div>
                                                <div class="tm-radial uk-float-left" data-circle-value="1">
                                                    <div>Costo <span>$80</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="jcomments-links"><a class="readmore-link" href="curso/" title="Swimming">Mas información ...</a>  </div>
                            </div>
                        </article>
                        <!-- END Article 2 block -->
                    </div>
                    <div class="uk-width-medium-1-2">
                        <!-- START Article 3 block -->
                        <article class="uk-article">
                            <h2 class="uk-article-title">
                                <a href="curso/" title="Swimming">Bailoterapia</a>
                            </h2>
                            <div>
                                <div class="uk-panel tm-classes">
                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('<?= base_url() ?>assets/images/demo/classes/class-6.jpg');">
                                        <img src="<?= base_url() ?>assets/images/demo/classes/class-6.jpg" class="uk-invisible" width="610" height="610" alt="5 Unique Workauts to Improve your Deadlift">
                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom">
                                            <div class="ak-overlay-classes uk-width-1-1">
                                                <div class="tm-radial uk-float-left" data-circle-value="1">
                                                    <div>Duración <span>60 minutes</span></div>
                                                </div>
                                                <div class="tm-radial uk-float-left" data-circle-value="1">
                                                    <div>Costo <span>$80</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="jcomments-links"><a class="readmore-link" href="curso/" title="Swimming">Mas información ...</a>  </div>
                            </div>
                        </article>
                        <!-- END Article 3 block -->
                        
                        <!-- START Article 4 block -->
                        <article class="uk-article">
                            <h2 class="uk-article-title">
                                <a href="curso/" title="Swimming">Bailes Tropicales</a>
                            </h2>
                            <div>
                                <div class="uk-panel tm-classes">
                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('<?= base_url() ?>assets/images/demo/classes/class-6.jpg');">
                                        <img src="<?= base_url() ?>assets/images/demo/classes/class-6.jpg" class="uk-invisible" width="610" height="610" alt="5 Unique Workauts to Improve your Deadlift">
                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom">
                                            <div class="ak-overlay-classes uk-width-1-1">
                                                <div class="tm-radial uk-float-left" data-circle-value="1">
                                                    <div>Duración <span>60 minutes</span></div>
                                                </div>
                                                <div class="tm-radial uk-float-left" data-circle-value="1">
                                                    <div>Costo <span>$80</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="jcomments-links"><a class="readmore-link" href="curso/" title="Swimming">Mas información ...</a>  </div>
                            </div>
                        </article>
                        <!-- END Article 4 block -->
                    </div>
                    @endif
                </div>
                <!-- END Article Blog block -->
            </main>
        </div>
    </div>
</div>
@endsection