@extends('template/base')

@section('content')
<div class="uk-container uk-container-center">
    <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
        <div class="tm-main uk-width-medium-1-1">
            <main class="tm-content uk-position-relative">
                <br>
                <div id="system-message-container"></div>
                
                <!-- START Article block -->
                <article class="uk-article">
                    <h1 class="uk-article-title">
                        Horarios
                    </h1>
                    <div>
                        <p>
                            <img class="uk-thumbnail uk-float-right uk-margin-left" src="<?= base_url() ?>assets/images/demo/visuals/schedule.jpg" alt="Schedule demo">
                            <br>
                            <br>
                            La Academia Fuego posee diferentes horarios en cada una de sus <br>
                            disciplina a escoger por los usuarios que quieran aprendar a <br>
                            bailar diferentes ritmos
                        </p>
                        <br><br><br><br>
                        <div>
                            <!-- START Schedule block -->
                            <div class="uk-position-relative">
                                <div class="akmf-scroll">
                                    <div class="akmf-header-placeholder gblue">&nbsp;</div>
                                    <div class="akmf-dayname">
                                        <h3 class="uk-margin-remove gwhite">Lunes</h3>
                                    </div>
                                    <div class="akmf-dayname">
                                        <h3 class="uk-margin-remove ggrey">Martes</h3>
                                    </div>
                                    <div class="akmf-dayname">
                                        <h3 class="uk-margin-remove gwhite">Miercoles</h3>
                                    </div>
                                    <div class="akmf-dayname">
                                        <h3 class="uk-margin-remove ggrey">Jueves</h3>
                                    </div>
                                    <div class="akmf-dayname">
                                        <h3 class="uk-margin-remove gwhite">Viernes</h3>
                                    </div>
                                </div>
                                <div class="akmf-box" tabindex="0">
                                    <div class="akmf">
                                        <div class="akmf-header uk-vertical-align">
                                            <div class="placeholder">&nbsp;</div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">00:00</span><span class="half">07:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">01:00</span><span class="half">07:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">02:00</span><span class="half">07:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">03:00</span><span class="half">07:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">04:00</span><span class="half">07:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">05:00</span><span class="half">07:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">06:00</span><span class="half">07:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">07:00</span><span class="half">07:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">08:00</span><span class="half">08:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">09:00</span><span class="half">09:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">10:00</span><span class="half">10:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">11:00</span><span class="half">11:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">12:00</span><span class="half">12:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">13:00</span><span class="half">13:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">14:00</span><span class="half">14:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">15:00</span><span class="half">15:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">16:00</span><span class="half">16:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">17:00</span><span class="half">17:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">18:00</span><span class="half">18:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">19:00</span><span class="half">19:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">20:00</span><span class="half">20:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">21:00</span><span class="half">21:30</span></div>
                                            <div class="uk-vertical-align-bottom"><span class="hour">22:00</span><span class="half">22:30</span></div>
                                        </div>
                                        <?php $horarios = Horarios::all()->where('estado', '1') ?>
                                        <div class="akmf-row">
                                            <div class="akmf-dayname">&nbsp;</div>
                                            <div class="akmf-day akmf-monday">
                                                @foreach ($horarios as $row)
                                                <div class="akmf-class start-time-{{ explode(':',$row->hora_inicio)[0] }}-{{ explode(':',$row->hora_inicio)[1] }} end-time-{{ explode(':',$row->hora_fin)[0] }}-{{ explode(':',$row->hora_fin)[1] }}">{{ $row->curso()->disciplina }}</div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="akmf-row">
                                            <div class="akmf-dayname">&nbsp;</div>
                                            <div class="akmf-day akmf-tuesday">
                                                @foreach ($horarios as $row)
                                                <div class="akmf-class start-time-{{ explode(':',$row->hora_inicio)[0] }}-{{ explode(':',$row->hora_inicio)[1] }} end-time-{{ explode(':',$row->hora_fin)[0] }}-{{ explode(':',$row->hora_fin)[1] }}">{{ $row->curso()->disciplina }}</div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="akmf-row">
                                            <div class="akmf-dayname">&nbsp;</div>
                                            <div class="akmf-day akmf-wednesday">
                                                @foreach ($horarios as $row)
                                                <div class="akmf-class start-time-{{ explode(':',$row->hora_inicio)[0] }}-{{ explode(':',$row->hora_inicio)[1] }} end-time-{{ explode(':',$row->hora_fin)[0] }}-{{ explode(':',$row->hora_fin)[1] }}">{{ $row->curso()->disciplina }}</div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="akmf-row">
                                            <div class="akmf-dayname">&nbsp;</div>
                                            <div class="akmf-day akmf-thursday">
                                                @foreach ($horarios as $row)
                                                <div class="akmf-class start-time-{{ explode(':',$row->hora_inicio)[0] }}-{{ explode(':',$row->hora_inicio)[1] }} end-time-{{ explode(':',$row->hora_fin)[0] }}-{{ explode(':',$row->hora_fin)[1] }}">{{ $row->curso()->disciplina }}</div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="akmf-row">
                                            <div class="akmf-dayname">&nbsp;</div>
                                            <div class="akmf-day akmf-friday">
                                                @foreach ($horarios as $row)
                                                <div class="akmf-class start-time-{{ explode(':',$row->hora_inicio)[0] }}-{{ explode(':',$row->hora_inicio)[1] }} end-time-{{ explode(':',$row->hora_fin)[0] }}-{{ explode(':',$row->hora_fin)[1] }}">{{ $row->curso()->disciplina }}</div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- END Schedule block -->
                        </div>
                    </div>
                </article>
                <!-- END Article block -->
            </main>
        </div>
    </div>
</div>
@endsection