@extends('template/base')

@section('style')
<!-- Gallery Styles -->
<link href="<?php base_url(); ?>assets/css/gallery.css" rel="stylesheet">
@endsection

@section('content')
<div class="uk-container uk-container-center">
    <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
        <div class="tm-main uk-width-medium-1-1">
            <main class="tm-content uk-position-relative">
                <br>
                <div id="system-message-container"></div>
                
                <!-- START Gallery block -->
                <article class="uk-article">
                    <h1 class="uk-article-title">
                        Gallery
                    </h1>
                    <div>
                        <div>
                            <div id="rg-151" class="rokgallery-wrapper">
                                <div class="rg-gm-container cols3">
                                    <div class="rg-gm-slice-container">
                                        <ul class="rg-gm-slice-list">
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="Armwrestling" href="<?php base_url(); ?>assets/images/gallery/gallery_1.jpg">
                                                            <img title="Armwrestling" alt="Armwrestling" src="<?php base_url(); ?>assets/images/gallery/gallery_1_thumb.jpg" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Armwrestling</span>
                                                        <span class="rg-gm-caption">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. </span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="The Dumbbell Workout" href="<?php base_url(); ?>assets/images/gallery/gallery_2.jpg">
                                                             <img title="The Dumbbell Workout" alt="The Dumbbell Workout" src="<?php base_url(); ?>assets/images/gallery/gallery_2_thumb.jpg" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">The Dumbbell Workout</span>
                                                        <span class="rg-gm-caption">Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="The Dumbbell Workout 2" href="<?php base_url(); ?>assets/images/gallery/gallery_3.jpg">
                                                            <img title="The Dumbbell Workout 2" alt="The Dumbbell Workout 2" src="<?php base_url(); ?>assets/images/gallery/gallery_3_thumb.jpg" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">The Dumbbell Workout 2</span>
                                                        <span class="rg-gm-caption">Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.</span>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="rg-gm-slice-list">
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="Workout Routines" href="<?php base_url(); ?>assets/images/gallery/gallery_4.jpg">
                                                            <img title="Workout Routines" alt="Workout Routines" src="<?php base_url(); ?>assets/images/gallery/gallery_4_thumb.jpg" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Workout Routines</span>
                                                        <span class="rg-gm-caption">Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="Tennis" href="<?php base_url(); ?>assets/images/gallery/gallery_5.jpg">
                                                            <img title="Tennis" alt="Tennis" src="<?php base_url(); ?>assets/images/gallery/gallery_5_thumb.jpg" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Tennis</span>
                                                        <span class="rg-gm-caption">A game in which two or four players strike a ball with rackets over a net stretched across a court. </span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="Training with rope" href="<?php base_url(); ?>assets/images/gallery/gallery_6.jpg">
                                                            <img title="Training with rope" alt="Training with rope" src="<?php base_url(); ?>assets/images/gallery/gallery_6_thumb.jpg" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Training with rope</span>
                                                        <span class="rg-gm-caption">Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo.</span>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="rg-gm-slice-list">
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="Workouts 2" href="<?php base_url(); ?>assets/images/gallery/gallery_7.jpg">
                                                            <img title="Workouts 2" alt="Workouts 2" src="<?php base_url(); ?>assets/images/gallery/gallery_7_thumb.jpg" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Workouts 2</span>
                                                        <span class="rg-gm-caption">Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet.</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="Athletics" href="images/gallery/gallery_8.jpg">
                                                            <img title="Athletics" alt="Athletics" src="<?php base_url(); ?>assets/images/gallery/gallery_8_thumb.jpg" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Athletics</span>
                                                        <span class="rg-gm-caption">Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, tellus.</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="Workouts" href="images/gallery/gallery_9.jpg">
                                                            <img title="Workouts" alt="Workouts" src="<?php base_url(); ?>assets/images/gallery/gallery_9_thumb.jpg" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Workouts</span>
                                                        <span class="rg-gm-caption">A session of vigorous physical exercise or training.</span>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- END Gallery block -->
            </main>
        </div>
    </div>
</div>
@endsection