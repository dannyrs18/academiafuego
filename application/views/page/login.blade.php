<html lang="es">
<head>

    <title> Transparent Login Form Design </title>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/style.css">   
    <meta charset="utf-8">
</head>
    <body>
    <header>
        <h1 class="title">LOGIN - ACADEMIA FUEGO</h1>
        <a href="/page/sing_up">Registrate</a>
    </header>
    <div class="login-box">
    <img src="<?= base_url(); ?>assets/images/avatar.png" class="avatar">
        <h1>Iniciar Sesiòn</h1>

        <?php echo validation_errors(); ?>

        <?php echo form_open(''); ?>
            <label for="usuario"><p>Correo o nombre de usuario</p></label> 
            <input type="text" name="user" placeholder="Usuario" id="usuario" value="<?= set_value('usuario') ?>">
            <label for="password"><p>Contraseña</p></label> 
            <input type="password" name="password" placeholder="Contraseña" id="password" >

            <input type="submit" name="submit" value="Aceptar">

            </form>
        
        </div>
    
    </body>
</html>