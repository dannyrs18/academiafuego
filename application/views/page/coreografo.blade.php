@extends('template/base')

@section('content')
<div class="uk-container uk-container-center">
    <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
        <div class="tm-main uk-width-medium-1-1">
            <main class="tm-content uk-position-relative">
                <br>
                
                <div id="system-message-container"></div>
                
                <div class="uk-grid">
                    <div class="uk-width-1-1">
                        <div class="uk-panel uk-panel-header">
                            <h1 class="tm-title">Coreógrafos</h1>
                        </div>
                    </div>
                </div>
                
                
                
                <!-- START Article block -->
                <div class="uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
                    <?php 
                        $data = array();
                        foreach (Perfiles::all()->where('estado', '1')->where('level_to', '3') as $value) {
                            array_push($data, $value);
                        }
                        $contador=0
                    ?>
                    @if (count($data))
                    <div class="uk-width-medium-1-2">
                        @for ($i = 0; $i < intval(count($data))/2; $i++)
                        <article class="uk-article">
                            <h2 class="uk-article-title">
                                <a href="trainers/edward-j-hopper.html" title="Edward J. Hopper">{{ explode(' ',$data[$i]['nombres'])[0].' '.explode(' ', $data[$i]['apellidos'])[0] }}</a>
                            </h2>
                            @if ($data[$i]['img_perfil'])
                            <a class="uk-align-left" href="trainers/edward-j-hopper.html" title=""><img src="<?= base_url() ?>uploads/{{ $data[$i]['img_perfil'] }}" alt=""></a>
                            @else
                            <a class="uk-align-left" href="trainers/edward-j-hopper.html" title=""><img src="<?= base_url() ?>assets/images/demo/trainers/trainer-1.jpg" alt=""></a>
                            @endif
                            <div>
                                <div class="jcomments-links">
                                    <a class="readmore-link" href="/coreografo/{{ $data[$i]['slug'] }}" title="{{ explode(' ',$data[$i]['nombres'])[0].' '.explode(' ', $data[$i]['apellidos'])[0] }}">Mas Información...</a>
                                </div>
                            </div>
                        </article>
                        <?php $contador = $i+1 ?>
                        @endfor
                    </div>
                    <div class="uk-width-medium-1-2">
                        @for ($i = $contador; $i < intval(count($data)); $i++)
                        <article class="uk-article">
                            <h2 class="uk-article-title">
                                <a href="trainers/edward-j-hopper.html" title="Edward J. Hopper">Edward J. Hopper</a>
                            </h2>
                            <a class="uk-align-left" href="trainers/edward-j-hopper.html" title=""><img src="<?= base_url() ?>assets/images/demo/trainers/trainer-1.jpg" alt=""></a>
                            <div>
                                <div class="jcomments-links">
                                    <a class="readmore-link" href="coreografo/Edwar" title="Edward J. Hopper">Mas Información...</a>
                                </div>
                            </div>
                        </article>
                        @endfor
                    </div>    
                    @else
                    <div class="uk-width-medium-1-2">
                        <article class="uk-article">
                            <h2 class="uk-article-title">
                                <a href="trainers/edward-j-hopper.html" title="Edward J. Hopper">Edward J. Hopper</a>
                            </h2>
                            <a class="uk-align-left" href="trainers/edward-j-hopper.html" title=""><img src="<?= base_url() ?>assets/images/demo/trainers/trainer-1.jpg" alt=""></a>
                            <div>
                                <div class="jcomments-links">
                                    <a class="readmore-link" href="coreografo/Edwar" title="Edward J. Hopper">Mas Información...</a>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="uk-width-medium-1-2" style="min-height: 1671px;">
                        <article class="uk-article">
                            <h2 class="uk-article-title">
                                <a href="trainers/edward-j-hopper.html" title="Amelie Noble">Amelie Noble</a>
                            </h2>
                            <a class="uk-align-left" href="trainers/edward-j-hopper.html" title=""><img src="<?= base_url() ?>assets/images/demo/trainers/trainer-4.jpg" alt=""></a>
                            <div>
                                <div class="jcomments-links">
                                    <a class="readmore-link" href="trainers/edward-j-hopper.html" title="Edward J. Hopper">Mas Información...</a>
                                </div>
                            </div>
                        </article>
                    </div>
                    @endif
                </div>
                <!-- END Article block -->
            </main>
        </div>
    </div>
</div>
@endsection