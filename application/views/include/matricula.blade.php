<table id="datatable" class="table table-striped table-bordered text-center">
    <thead>
        <tr>
            <th>#</th>
            <th>Fecha a Pagar</th>
            <th>Cuota</th>
            <th>Estado</th>
        </tr>
    </thead>
    <tbody>
        <?php setlocale(LC_ALL,"es_EC.utf8"); ?>
        <?php $fecha = $horario->fecha_inicio ?>
        @for ($i = 1; $i <= $horario->duracion; $i++)
        <tr>
            <td>{{ $i }}</td>
            <td><?= strftime("%A %e %B %Y", strtotime($fecha)); ?></td>
            <td>{{ money_format('%.2n', $horario->costo/$horario->duracion) }}</td>
            <td class="danger">Pendiente</td>
        </tr>
        <?php $fecha = date("Y-m-d", strtotime(date('Y-m-d', strtotime($fecha))." + 1 months")) ?>
        @endfor
    </tbody>
</table>