<div class="ak-footer">
    <div class="uk-container uk-container-center">
        <footer class="tm-footer uk-grid tm-footer uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
            <!-- START Footer About MaxxFitness block -->
            <div class="uk-hidden-small uk-hidden-medium uk-width-large-1-2">
                <div class="uk-panel uk-hidden-medium uk-hidden-small" style="min-height: 316px;">
                    <h3 class="uk-panel-title uk-text-center">Academia Fuego</h3>
                    <p class="uk-text-justify">Fomentar el arte del baile a través de la Unión de profesionales dispuestos a brindar un servicio con calidad en la Enseñanza a nuestros alumnos con responsabilidad y compromiso para Generar ideas innovadoras que desarrollen su talento.</p>
                    <p> Dir: Calle Lourdes entre<br> Juan Josè Peña y 24 de Mayo<br> Contactos: 0991364690</p>
                    <div class="ak-copyright">©AcadFuego 2018 - 2019</div>
                </div>
            </div>
            <!-- END Footer About MaxxFitness block -->
            
            <!-- START Opening Hours block -->
            <div class="uk-width-1-1 uk-width-medium-1-2 uk-width-large-1-2">
                <div class="uk-panel" style="min-height: 316px;">
                    <h3 class="uk-panel-title uk-text-center">Horarios de Atención</h3>
                    <div class="uk-grid">
                        <div class="uk-width-1-2 uk-text-center">
                            <p style="line-height: 2;">
                                _______ Lunes _______<br>
                                ______ Martes ______<br>
                                ____ Miercoles _____<br>
                                ______ Jueves ______<br>
                                ______ Viernes ______
                            </p>
                        </div>
                        <div class="uk-width-1-2 uk-text-center">
                            <p style="line-height: 2;">
                                15:00AM - 19:00PM<br>
                                15:00AM - 19:00PM<br>
                                15:00AM - 19:00PM<br>
                                15:00AM - 19:00PM<br>
                                15:00AM - 19:00PM
                            </p>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div id="aiContactSafe_form_1" class="uk-text-center">
                        <a href="/page/inscripcion" id="aiContactSafeSendButton" style="margin:35%;"><strong>INSCRÍBETE</strong></a>
                    </div>
                </div>
            </div>
            <!-- END Opening Hours block -->
            
        </footer>
    </div>
</div>