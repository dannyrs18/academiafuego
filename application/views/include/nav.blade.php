<li @if($page=='inicio')class="uk-active"@endif><a href="/">Inicio</a></li>
<li @if($page=='acerca')class="uk-active"@endif><a href="/acerca">Acerca</a></li>
<li @if($page=='horario')class="uk-active"@endif><a href="/horario">Horarios</a></li>
<li class="uk-parent" data-uk-dropdown="{}" aria-haspopup="true" aria-expanded="false">
    <a href="/curso">Cursos</a>
    <div class="uk-dropdown uk-dropdown-navbar">
        <div class="uk-grid uk-dropdown-grid">
            <div class="uk-width-1-1">
                <ul class="uk-nav uk-nav-navbar">
                    @foreach (Cursos::all()->where('estado', '1') as $row)
                    <li><a href="/curso/{{ $row->slug }}">{{ $row->disciplina }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</li>
@if (isset($_SESSION['level_to']))
    @if ($_SESSION['level_to'] == '4')
    <li @if($page=='inscripcion')class="uk-active"@endif><a href="/page/inscripcion">Pre-Inscripciones</a></li>
    @endif
@else
<li @if($page=='inscripcion')class="uk-active"@endif><a href="/page/inscripcion">Pre-Inscripciones</a></li>
@endif
<li @if($page=='coreografo')class="uk-active"@endif><a href="/coreografo">Coreógrafos</a></li>
<li @if($page=='galeria')class="uk-active"@endif><a href="/galeria">Galeria</a></li>
<li @if($page=='loxalizacion')class="uk-active"@endif><a href="/localizacion">Localización</a></li>
@if (empty($_SESSION['username']))
<li><a href="/page/login">Login</a></li>
@else
<li><a href="/admin/inicio">administración</a></li>
@endif