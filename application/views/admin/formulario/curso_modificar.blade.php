@extends('template/base_admin')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> <strong>{{ $title }}</strong></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <?php echo validation_errors(); ?>

                    <?php 
                    $attr = array(
                        'id' => "demo-form2",
                        'data-parsley-validate' => "",
                        'class' =>"form-horizontal form-label-left"
                    );
                    echo form_open_multipart('', $attr); 
                    ?>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="discipina">Nombre de la disciplina<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="disciplina" name="disciplina" required="required" class="form-control col-md-7 col-xs-12" value="@if (set_value('disciplina')){{ set_value('disciplina') }}@else{{ $instance->disciplina }}@endif">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descripcion">Descripción<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control" id="descripcion" rows=5 name="descripcion" required="required">@if (set_value('descripcion')){{ set_value('descripcion') }}@else{{ $instance->descripcion }}@endif</textarea>
                            </div>
                        </div>
            
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="img_curso">Imagen de Curso</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="file" id="imagen" name="img_curso" class="form-control col-md-7 col-xs-12" accept="image/*">
                            </div>
                        </div>
            
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="/admin/inicio" class="btn btn-primary">Cancelar</a>
                                <input type="reset" value="Limpiar" class="btn btn-primary">
                                <input type="submit" value="Enviar" class="btn btn-success">
                            </div>
                        </div>
    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection