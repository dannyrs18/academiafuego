@extends('template/base_admin')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> <strong>{{ $title }}</strong></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <div class="form-horizontal form-label-left" data-parsley-validate id="demo-form2">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="cedula">Cédula</label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input type="number" class="form-control has-feedback-left" id="cedula" placeholder="Ingrese número de cédula">
                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>
                            <input type="button" value="Buscar" class="btn btn-default" id="buscar">
                        </div>    
                        <br>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nombres">Nombres</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" id="nombres" class="form-control col-md-7 col-xs-12" readonly value="">
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="apellidos">Apellidos</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" id="apellidos" class="form-control col-md-7 col-xs-12" readonly value="">
                            </div>
                        </div>
    
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="discipina">Curso</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" id="curso" class="form-control col-md-7 col-xs-12" readonly value="">
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="horario">Horario</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" id="horario" class="form-control col-md-7 col-xs-12" readonly value="">
                            </div>
                        </div>
    
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="costo">Costo<span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                            <input type="text" id="costo" class="form-control col-md-7 col-xs-12" readonly value="">
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="pagado">Pagado<span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                            <input type="text" id="pagado" class="form-control col-md-7 col-xs-12" readonly value="">
                            </div>
                        </div>

                    </div>
                    <br>
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12" id="tabla_pension">
                        <!-- Ajax -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $('#buscar').click(function (){
                $('#nombres').val('');
                $('#apellidos').val('');
                $('#curso').val('');
                $('#horario').val('');
                $('#costo').val('');
                $('#pagado').val('');
                if ($('#cedula').val()){
                    $.ajax({
                        data: {'cedula' : $('#cedula').val()},
                        url: "<?= base_url() ?>ajax/template_pension",
                        type: 'post',
                        dataType: 'html',
                        success: function(data){ 
                            $('#tabla_pension').html(data);
                        }
                    })
                }
            })
        })
    </script>
@endsection