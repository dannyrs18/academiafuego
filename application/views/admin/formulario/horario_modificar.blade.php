@extends('template/base_admin')

@section('style')
<!-- bootstrap-daterangepicker -->
<link href="<?= base_url() ?>assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> <strong>{{ $title }}</strong></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <?php echo validation_errors(); ?>

                    <?php 
                    $attr = array(
                        'id'                    => "demo-form2",
                        'data-parsley-validate' => "",
                        'class'                 =>"form-horizontal form-label-left"
                    );
                    echo form_open('', $attr); 
                    ?>  
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="coreografo">Coreografo </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="coreografo" class="form-control" name="coreografo" required>
                                    <option value="">----------</option>
                                    @foreach (Perfiles::all()->where('level_to', '3') as $row)
                                    @if ($row->id == $instance->coreografo_id)
                                    <option value={{ $row->id }} selected>{{ $row->get_full_name() }}</option>
                                    @else
                                    <option value={{ $row->id }}>{{ $row->get_full_name() }}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_inicio">Fecha Inicio<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" required="required" class="form-control has-feedback-left fecha" name="fecha_inicio" id="fecha_inicio" aria-describedby="inputSuccess2Status3" value="@if (set_value('fecha_inicio')){{ set_value('fecha_inicio') }}@else{{ $instance->fecha_inicio }}@endif">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="inicio">Hora de inicio<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="time" id="inicio" name="inicio" required="required" class="form-control col-md-7 col-xs-12" value="@if (set_value('inicio')){{ set_value('inicio') }}@else{{ $instance->hora_inicio }}@endif">
                            </div>
                        </div>
            
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fin">Hora de culminación<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="time" id="fin" name="fin" required="required" class="form-control col-md-7 col-xs-12" value="@if (set_value('fin')){{ set_value('fin') }}@else{{ $instance->hora_fin }}@endif">
                            </div>
                        </div>
            
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="/admin/inicio" class="btn btn-primary">Cancelar</a>
                                <input type="reset" value="Limpiar" class="btn btn-primary">
                                <input type="submit" value="Enviar" class="btn btn-success">
                            </div>
                        </div>
    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<!-- bootstrap-daterangepicker -->
<script src="<?= base_url() ?>assets/vendors/moment/min/moment.min.js"></script>
<script src="<?= base_url() ?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
    $(function(){
        $('.fecha').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD-MM-YYYY'
            },
        })

    });
</script>
@endsection