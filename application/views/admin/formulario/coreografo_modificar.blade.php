@extends('template/base_admin')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> <strong>{{ $title }}</strong></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <?php echo validation_errors(); ?>

                    @php
                        $attr = array(
                        'id' => "demo-form2",
                        'data-parsley-validate' => "",
                        'class' =>"form-horizontal form-label-left"
                        );
                        echo form_open_multipart('', $attr); 
                    @endphp
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">Nombres<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="nombres" name="nombres" required="required" class="form-control col-md-7 col-xs-12" value="@if (set_value('nombres')){{ set_value('nombres') }}@else{{ $instance->nombres }}@endif">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="apellidos">Apellidos<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="apellidos" name="apellidos" required="required" class="form-control col-md-7 col-xs-12" value="@if (set_value('apellidos')){{ set_value('apellidos') }}@else{{ $instance->apellidos }}@endif">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="informacion">Información<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea id="informacion" name="informacion" required="required" class="form-control col-md-7 col-xs-12" rows="3">@if (set_value('informacion')){{ set_value('informacion') }}@else{{ $instance->informacion }}@endif</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="direccion">Direccion<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="direccion" name="direccion" required="required" class="form-control col-md-7 col-xs-12" value="@if (set_value('direccion')){{ set_value('direccion') }}@else{{ $instance->direccion }}@endif">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">Telefono<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="telefono" name="telefono" required="required" class="form-control col-md-7 col-xs-12" value="@if (set_value('telefono')){{ set_value('telefono') }}@else{{ $instance->telefono }}@endif">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="imagen">Imagen de Perfil</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="file" id="imagen" name="img_perfil" class="form-control col-md-7 col-xs-12" accept="image/*">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Contraseña<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="password" name="password" class="form-control col-md-7 col-xs-12" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password2">Repita la contraseña<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="password2" name="password2" class="form-control col-md-7 col-xs-12" value="">
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button class="btn btn-primary" type="button">Cancelar</button>
                            <button class="btn btn-primary" type="reset">Limpiar</button>
                            <button type="submit" class="btn btn-success">Enviar</button>
                        </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection