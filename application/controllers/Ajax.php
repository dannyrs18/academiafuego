<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Ajax extends MY_Controller {

        public function __construct(){

            parent::__construct();
            if(!$this->session->username){
                redirect('/inicio');
            }

        }

        /**
         * @method Obtener todos los cursos de la base de datos por ajax
         * @param null 
         */
        public function curso(){
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                echo Cursos::all();
            }else {
                redirect('/inicio');
            }
        }

        /**
         * @method Obtener todos los horarios de la base de datos por ajax
         * @param null
         */
        public function horario(){
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                echo Horarios::all();
            }else {
                redirect('/inicio');
            }
        }

        /**
         * @method Obtener todos las inscripciones de la base de datos por ajax
         * @param null
         */
        public function inscripcion(){
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                echo Inscripciones::all();
            }else {
                redirect('/inicio');
            }
        }

        /**
         * @method Obtener todas las matriculas de la base de datos por ajax
         * @param null 
         */
        public function matricula(){
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                echo Matriculas::all();
            }else {
                redirect('/inicio');
            }
        }

        /**
         * @method Obtener todas las pensiones de la base de datos por ajax
         * @param null
         */
        public function pension(){
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                echo Pensiones::all();
            }else {
                redirect('/inicio');
            }
        }

        /**
         * @method calcular la cantidad de cuotas de la matricula y devolverlo en template
         * @param null
         */
        public function template_matricula(){
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                $data = array(
                    'horario' => Horarios::where('slug', $this->input->post('slug', TRUE))->first()
                );
                echo $this->blade->view()->make("include/matricula", $data);
            }else {
                redirect('/inicio');
            }
        }

        /**
         * @method presentar las pensiones por matricula y devolverlo en template
         * @param null
         */
        public function template_pension(){
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                $matriculas = array();
                foreach (Matriculas::all() as $value) {
                    if ($value->inscripcion()->cliente()->cedula == $this->input->post('cedula')) {
                        if ($value->pensiones()){
                            array_push($matriculas, $value);
                        }
                    }
                }
                $data = array(
                    'matricula' => $matriculas
                );
                echo $this->blade->view()->make("include/pension", $data);
            }else {
                redirect('/inicio');
            }
        }

    }