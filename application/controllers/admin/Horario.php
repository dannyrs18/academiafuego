<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Horario extends MY_Controller {

    public function __construct(){

        parent::__construct();
        if (!$this->session->username){
            redirect('/inicio');
        }

    }

    /**
	 * @method: CREATE DATA horario IN DATABASE
	 * @param null
	 */
    public function post(){
        $level = array(1, 2);
        if(in_array($this->session->level_to, $level)){

            $data = array(
                'title' => 'CREAR HORARIO',
                'cursos' => Cursos::all(),
                'perfiles' => Perfiles::all()->where('level_to', 3)->where('estado', 1)
            );
            $this->form_validation->set_rules('curso', 'Curso', array('trim','required', array(
                'option', function($opt){
                    if(Cursos::find($opt)){
                        return TRUE;
                    }
                    $this->form_validation->set_message('option', 'Ingrese {field} valida');
                    return FALSE;
                }
            )));
            $this->form_validation->set_rules('coreografo', 'Coreografo', array('trim','required', array(
                'option', function($opt){
                    if(Perfiles::all()->where('id', $opt)->where('level_to', '3')){
                        return TRUE;
                    }
                    $this->form_validation->set_message('option', 'Ingrese {field} valida');
                    return FALSE;
                }
            )));
            $this->form_validation->set_rules('tipo', 'Tipo', array('trim','required', array(
                'option', function($opt){
                    if($opt > 0 && $opt < 4){
                        return TRUE;
                    }
                    $this->form_validation->set_message('option', 'No existe la opcion seleccionada');
                    return FALSE;
                }
            )));
            $this->form_validation->set_rules('inicio', 'Fecha de inicio', array('trim', 'required', array(
                'date_valid', function($fecha){
                    $valores = explode('-', $fecha);
                    if(count($valores) == 3 && checkdate($valores[1], $valores[0], $valores[2]) % $fecha > date()){
                        return TRUE;
                    }
                    $this->form_validation->set_message('date_valid', 'Ingrese una fecha valida');
                    return FALSE;
                }
            )));
            $this->form_validation->set_rules('fecha_inicio', 'Hora de inicio', 'trim|required');
            $this->form_validation->set_rules('inicio', 'Hora de inicio', 'trim|required');
            $this->form_validation->set_rules('fin', 'Hora de culminación', 'trim|required');
            $this->form_validation->set_rules('cupo', 'Cupo', 'trim|required|integer|max_length[2]');
            $this->form_validation->set_rules('costo', 'Costo', 'trim|required|max_length[6]|regex_match[/^[0-9]+(\.[0-9]{1,2})?$/]');
            if (!$this->form_validation->run()){
                echo $this->blade->view()->make("admin/formulario/horario", $data);
            }else{
                $data = array(
                    'tipo_curso' => $this->input->post('tipo', TRUE),
                    'fecha_inicio' => date('Y-m-d', strtotime($this->input->post('fecha_inicio', TRUE))),
                    'hora_inicio' => $this->input->post('inicio', TRUE),
                    'hora_fin' => $this->input->post('fin', TRUE),
                    'costo' => $this->input->post('costo', TRUE),
                    'cupo' => $this->input->post('cupo', TRUE),
                    'estado' => '1',
                    'curso_id' => $this->input->post('curso', TRUE),
                    'coreografo_id' => $this->input->post('coreografo', TRUE),
                );
                $this->load->model('modelos');
                $this->modelos->horario($data);
                redirect('admin/inicio');
            }       
        }else{
            redirect('admin/inicio');
        }
        
    }

    public function get(){
        $level = array(1, 2, 3);
        if(in_array($this->session->level_to, $level)){
            $data = array(
                'title' => 'TABLA HORARIO',
            );
            echo $this->blade->view()->make("admin/tabla/horario", $data);
        }else{
            redirect('admin/inicio');
        }
    }

    public function put($slug){
		$level = array(1);
        if(in_array($this->session->level_to, $level)){
            $instance = Horarios::where('slug', $slug)->first();
            $data = array(
                'title' => strtoupper('MODIFICAR HORARIO'),
                'instance' => $instance
            );
            # Validation
            $this->form_validation->set_rules('coreografo', 'Coreografo', array('trim','required', array(
                'option', function($opt){
                    if(Perfiles::all()->where('id', $opt)->where('level_to', '3')){
                        return TRUE;
                    }
                    $this->form_validation->set_message('option', 'Ingrese {field} valida');
                    return FALSE;
                }
            )));
            $this->form_validation->set_rules('fecha_inicio', 'Hora de inicio', 'trim|required');
            $this->form_validation->set_rules('inicio', 'Hora de inicio', 'trim|required');
            $this->form_validation->set_rules('fin', 'Hora de culminación', 'trim|required');
            # /Validation
            if (!$this->form_validation->run()){
                echo $this->blade->view()->make("admin/formulario/horario_modificar", $data);
            }else{
                # Guardar
                $instance->coreografo_id = $this->input->post('coreografo', TRUE);
                $instance->fecha_inicio = date('Y-m-d', strtotime($this->input->post('fecha_inicio', TRUE)));
                $instance->fecha_fin = date("Y-m-d", strtotime(date('Y-m-d', strtotime($this->input->post('fecha_inicio', TRUE)))." + ".$instance->duracion." months"));
                $instance->hora_inicio = $this->input->post('inicio', TRUE);
                $instance->hora_fin = $this->input->post('fin', TRUE);
                $instance->save();
                # Guardar
                redirect('admin/inicio');
            }
        }else{
            redirect('admin/inicio');
        }
    }

    public function delete($slug){
		$level = array(1);
        if(in_array($this->session->level_to, $level)){
            $horario = Horarios::where('slug', $slug)->first();
            $horario->estado = ($horario->estado) ? '0' : '1' ;
            $horario->save();
            redirect('/admin/horario/tabla');
        }else{
            redirect('admin/inicio');
        }
    }

    public function report(){
        $level = array(1);
        if(in_array($this->session->level_to, $level)){
            $this->load->library('pdf');
            $this->pdf = new Pdf();
            $this->pdf->SetTitleHead('REPORTE CURSOS');
            $this->pdf->AddPage();
            $this->pdf->AliasNbPages();
            $info = array();
            $i=0;
            foreach (Horarios::all()->where('estado', '1') as $row){
                array_push($info, array(++$i, $row->curso()->disciplina, $row->coreografo()->get_simple_name(), $row->fecha_inicio, ($row->estado) ? 'Activo' : 'Inactivo'));
            }
            $data = array(
                'cabecera'=>array('Nro', 'Curso', 'Coreografo', 'Inicia', 'Horario'),
                'contenido'=>$info,
                'tamaño'=>array(15, 40, 80, 27, 27)
            );
            $this->pdf->head_table($data['cabecera'], $data['tamaño']);
            $this->pdf->SetWidths($data['tamaño']);
            $this->pdf->table($data['contenido']);
            $this->pdf->Output();
        }else{
            redirect('admin/inicio');
        }
    }
}