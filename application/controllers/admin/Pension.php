<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Pension extends MY_Controller {

    public function __construct(){

        parent::__construct();
        if (!$this->session->username){
            redirect('/inicio');
        }

    }

    /**
	 * CREATE DATA perfil IN DATABASE
	 * @param null
	 */
    public function post(){
        $level = array(1);
        if(in_array($this->session->level_to, $level)){
            $data = array(
                'title' => 'PAGO DE PENSIÓN'
            );
            echo $this->blade->view()->make("admin/formulario/pension", $data);
        }else{
            redirect('admin/inicio');
        }
    }

    public function get(){
        $level = array(1, 4);
        if(in_array($this->session->level_to, $level)){
            $data = array(
                'title' => 'TABLA PENSION',
            );
            echo $this->blade->view()->make("admin/tabla/pension", $data);
        }else{
            redirect('admin/inicio');
        }
    }

    public function put($slug){
        $level = array(1);
        if(in_array($this->session->level_to, $level)){
            $pension = Pensiones::where('slug', $slug)->first();
            $pension->estado = '0';
            $pension->fecha_pagado = date('Y-m-d');
            $pension->save();
            redirect('/admin/inicio');
        }else{
            redirect('admin/inicio');
        }
    }

    public function delete(){
		
    }
    
    public function report(){
        $level = array(1);
        if(in_array($this->session->level_to, $level)){
            $data = array(
                'title' => 'REPORTE PENSION'
            );
            $this->form_validation->set_rules('pension', 'Cliente', array('trim','required', array(
                'option', function($opt){
                    if(Perfiles::all()->where('id', $opt)->where('level_to', '4')){
                        return TRUE;
                    }
                    $this->form_validation->set_message('option', 'Ingrese {field} valida');
                    return FALSE;
                }
            )));
            if (!$this->form_validation->run()){
                echo $this->blade->view()->make("admin/formulario/pension_reporte", $data);
            }else{
                $matriculas = array();
                foreach (Matriculas::all() as $value) {
                    if ($value->inscripcion()->cliente()->id == $this->input->post('pension')) {
                        if ($value->pensiones()){
                            array_push($matriculas, $value);
                        }
                    }
                }
                $this->load->library('pdf');
                $this->pdf = new Pdf();
                $this->pdf->AddPage();
                $this->pdf->AliasNbPages();
                $info = array();
                $i=0;
                foreach ($matriculas as $value){
                    $horario = $value->inscripcion()->horario();
                    if ($value->estado){
                        foreach ($value->pensiones() as $row) {
                            array_push($info, array(++$i, $horario->curso()->disciplina, $row->fecha_pago, ($row->fecha_pagado=='0000-00-00') ? '' : '$row->fecha_pagado', ($row->estado) ? 'Pendiente' : 'Pagado'));
                        }
                    }
                }
                $data = array(
                    'cabecera'=>array('Nro', 'Curso', 'Pago', 'Pagado', 'Estado'),
                    'contenido'=>$info,
                    'tamaño'=>array(15, 45, 45, 43, 40)
                );
                $this->pdf->head_table($data['cabecera'], $data['tamaño']);
                $this->pdf->SetWidths($data['tamaño']);
                $this->pdf->table($data['contenido']);
                $this->pdf->Output();
            }
        }else{
            redirect('admin/inicio');
        }
        
    }
}