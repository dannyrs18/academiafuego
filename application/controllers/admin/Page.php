<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Page extends MY_Controller {

	public function __construct(){

        parent::__construct();
        if (!$this->session->username){
            redirect('/inicio');
        }

    }

    /**
	 * Pages
	 * @param page: name of basic template
	 */
	public function inicio(){
		echo $this->blade->view()->make('admin/inicio');
	}

	/**
	 * 
	 */
	public function contenido(){
		$data = array(
			'title' => 'CONTENIDO'
		);
		if (isset($_FILES['img'])){
			if (empty($_FILES['img'])){
				$GLOBALS['image_size'] = 90000;
				$GLOBALS['image_width'] = 1024;
				$GLOBALS['image_height'] = 768;
				$GLOBALS['image'] = $_FILES['img'];
			}
        }
		# Validacion
		$this->form_validation->set_rules('titulo', 'Titulo', 'trim|required');
		$this->form_validation->set_rules('descripcion', 'Descripción', 'trim|required');
		if (isset($_FILES['img'])){
			if (empty($_FILES['img'])){
				$this->form_validation->set_rules('img', 'Imagen', array('trim', array(
					'option', function($opt){
						$image = getimagesize($GLOBALS['image']['tmp_name']);
						if ($image[0] > $GLOBALS['image_width']){
							$this->form_validation->set_message('option', 'Ancho de {field} excedido');
							return FALSE;
						}else if ($image[1] > $GLOBALS['image_height']){
							$this->form_validation->set_message('option', 'Alto de {field} excedido');
							return FALSE;
						}else if ($GLOBALS['image']['size'] > $GLOBALS['image_size']){
							$this->form_validation->set_message('option', 'Peso de {field} excedido');
							return FALSE;
						}
						return TRUE;
					}
				)));
			}
		}
		# Validacion
		if (!$this->form_validation->run()){
			echo $this->blade->view()->make('admin/contenido', $data);
		}else{
			$img='';
			$file = 'contenido/';
            if ($_FILES['img']['name']){
                $config = array(
                    'upload_path'   => FCPATH.'/uploads/'.$file,
                    'allowed_types' => "jpg|png|jpeg",
                    'overwrite'     => false,
                    'max_size'      => $GLOBALS['image_size'],
                    'max_width'     => $GLOBALS['image_width'],
                    'max_height'    => $GLOBALS['image_height']
                );
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('img')){
                    $img = "{$file}{$this->upload->data('file_name')}";
                }
            }
			$data = array(
				'titulo' => $this->input->post('titulo'),
				'descripcion' => $this->input->post('descripcion'),
				'img' => $img
			);
			$this->load->model('modelos');
			$this->modelos->contenido($data);
			redirect('/admin/inicio');
		}
	}

	/**
	 * 
	 */
	public function respaldo(){
		$this->load->dbutil();

		$prefs = array(     
			'format'      => 'zip',             
			'filename'    => 'academia_fuego.sql'
			);

		$backup =& $this->dbutil->backup($prefs); 

		$db_name = 'backup-on-'. date("Y-m-d-H-i-s") .'.zip';
		$save = 'pathtobkfolder/'.$db_name;

		$this->load->helper('file');
		write_file($save, $backup); 


		$this->load->helper('download');
		force_download($db_name, $backup);
	}

	/**
	 * 
	 */
	public function logout(){
		$this->session->sess_destroy();
		redirect ('/inicio');
	}

}