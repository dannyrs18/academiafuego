<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Inscripcion extends MY_Controller {

    public function __construct(){

        parent::__construct();
        if (!$this->session->username){
            redirect('/inicio');
        }

    }

    /**
	 * Crea objeto inscripcion y guarda en base de datos
	 * @param user: role of user
	 */
    public function post(){
        
    }

    /**
     * @method Obtener todos los objetos inscripcion
     * @param null
     */
    public function get(){
        $level = array(1, 2, 4);
        if(in_array($this->session->level_to, $level)){
            $data = array(
                'title' => 'TABLA PRE-INSCRIPCIONES',
                'inscripciones' => Inscripciones::all()->where('estado', '1')
            );
            echo $this->blade->view()->make("admin/tabla/inscripcion", $data);
        }else{
            redirect('admin/inicio');
        }
    }

    public function put(){
		
    }

    public function delete(){
		
    }
    
    public function report(){
        $level = array(1);
        if(in_array($this->session->level_to, $level)){
            $this->load->library('pdf');
            $this->pdf = new Pdf();
            $this->pdf->SetTitleHead('REPORTE INSCRIPCIONES');
            $this->pdf->AddPage();
            $this->pdf->AliasNbPages();
            $info = array();
            foreach (Inscripciones::all()->where('estado', '1') as $row){
                array_push($info, array($row->cliente()->get_simple_name(), $row->cliente()->cedula, $row->horario()->curso()->disciplina, $row->horario()->coreografo()->get_simple_name(), money_format('%.2n', $row->horario()->costo), $row->fecha_inscripcion));
            }
            $data = array(
                'cabecera'=>array('Cliente', 'Cédula', 'Curso', 'Coreógrafo', 'Costo', 'Fecha'),
                'contenido'=>$info,
                'tamaño'=>array(43, 24, 35, 43, 20, 25)
            );
            $this->pdf->head_table($data['cabecera'], $data['tamaño']);
            $this->pdf->SetWidths($data['tamaño']);
            $this->pdf->table($data['contenido']);
            $this->pdf->Output();
        }else{
            redirect('admin/inicio');
        }
	}
}