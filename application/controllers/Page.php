<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MY_Controller {

	/**
	 * Pages
	 * @param page: name of basic template
	 */
	public function index($page='inicio'){
		if (!file_exists(APPPATH.'views/page/'.$page.'.blade.php')){
            show_404();
		}
		if (in_array($page, array('login', 'inscripcion', 'curso_informacion', 'coreografo_informacion', 'sing_up'))){
			show_404();
		}
		$data = array(
			'page' => $page
		);
		echo $this->blade->view()->make('page/'.$page, $data);
	}

	/**
	 * @method Course Information Page
	 * @param slug: slug course
	 */
	public function course($slug){
		$data = array(
			'page' => 'Curso',
			'curso' => Cursos::where('slug', $slug)->first()
		);
		echo $this->blade->view()->make('page/curso_informacion', $data);
	}

	/**
	 * @method Coreografo Information Page
	 * @param slug: slug coreografo
	 */
	public function coreografo($slug){
		$data = array(
			'page' => 'Coreografo',
			'coreografo' => Perfiles::where('slug', $slug)->first()
		);
		echo $this->blade->view()->make('page/coreografo_informacion', $data);
	}

	/**
	 * @method Pagina de Login
	 * @param null
	 */
	public function login(){
		$data = array(
			'page' => 'Login'
		);
		# Validation
		$this->form_validation->set_rules('user', 'Usuario', 'trim|required');
		$this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
		# Validation
		if (!$this->form_validation->run()){
			echo $this->blade->view()->make('page/login', $data);
		}else{
			if (Perfiles::where('username', $_POST['user'])->exists()){
				if(password_verify($_POST['password'], Perfiles::all()->where('username', $_POST['user'])->first()['password'])){
					$result = Perfiles::all()->where('username', $_POST['user'])->first();
					$sesion = array(
						'username'  => $result->username,
						'nombres'   => $result->nombres,
						'apellidos' => $result->apellidos,
						'id' => $result->id_perfil,
						'img_perfil' => $result->img_perfil,
						'level_to' => $result->level_to,
					);
					$this->session->set_userdata($sesion);
                    redirect('admin/inicio');
				}else{
					echo $this->blade->view()->make('page/login', $data);
				}
			}else{
				echo $this->blade->view()->make('page/login', $data);
			}
		}
	}

	/**
	 * 
	 */
	public function inscripcion(){
		if ($this->session->level_to == '4'){
			$perfil = Perfiles::all()->where('username', $this->session->username)->first();
			$data = array(
				'page' => 'incripcion',
				'fecha' => date("d-m-Y"),
				'cedula' => $perfil->cedula,
				'apellidos' => $perfil->apellidos,
				'nombres' => $perfil->nombres,
				'direccion' => $perfil->direccion,
				'telefono' => $perfil->telefono
			);
			# Validation
			$this->form_validation->set_rules('curso', 'Curso', array('trim', 'required', array(
				'option', function($opt){
					if(Cursos::find($opt)){
						return TRUE;
					}
					$this->form_validation->set_message('option', 'Ingrese {field} valida');
					return FALSE;
				})
			));
			
			$this->form_validation->set_rules('fecha_inicio', 'Fecha de inicio', array('trim', 'required', array(
				'option', function($opt){
					if(Horarios::find($opt)){
						return TRUE;
					}
					$this->form_validation->set_message('option', 'Ingrese {field} valida');
					return FALSE;
				})
			));
			# Validation
			if (!$this->form_validation->run()){
				echo $this->blade->view()->make('page/inscripcion', $data);
			}else{
				$data = array(
					'fecha_inscripcion' => date('Y-m-d'),
					'estado' => '1',
					'horario_id' => $this->input->post('fecha_inicio',TRUE),
					'cliente_id' => Perfiles::all()->where('username', $this->session->username)->first()['id']
				);
				$this->load->model('modelos');
				$this->modelos->inscripcion($data);
				redirect('/inicio');
			}
		}else{
			redirect('/page/login');
		}
	}

	/**
	 * 
	 */
	public function sing_up(){
		if (isset($_FILES['img_perfil'])){
			if (empty($_FILES['img_perfil'])){
				$GLOBALS['image_size'] = 90000;
				$GLOBALS['image_width'] = 1024;
				$GLOBALS['image_height'] = 768;
				$GLOBALS['image'] = $_FILES['img_perfil'];
			}
        }
        # Validation
        $this->form_validation->set_rules('cedula', 'Cedula', array('trim', 'required', 'integer', 'max_length[10]', 'min_length[10]', 'is_unique[perfil.cedula]', array(
			'option', function($opt){
				$ult_digito=substr($opt, -1,1);//extraigo el ultimo digito de la cedula
				//extraigo los valores pares//
				$valor2=substr($opt, 1, 1);
				$valor4=substr($opt, 3, 1);
				$valor6=substr($opt, 5, 1);
				$valor8=substr($opt, 7, 1);
				$suma_pares=($valor2 + $valor4 + $valor6 + $valor8);
				//extraigo los valores impares//
				$valor1=substr($opt, 0, 1);
				$valor1=($valor1 * 2);
				if($valor1>9){ 
					$valor1=($valor1 - 9); 
				}
				$valor3=substr($opt, 2, 1);
				$valor3=($valor3 * 2);
				if($valor3>9){ 
					$valor3=($valor3 - 9); 
				}
				$valor5=substr($opt, 4, 1);
				$valor5=($valor5 * 2);
				if($valor5>9){ 
					$valor5=($valor5 - 9); 
				}
				$valor7=substr($opt, 6, 1);
				$valor7=($valor7 * 2);
				if($valor7>9){ 
					$valor7=($valor7 - 9); 
				}
				$valor9=substr($opt, 8, 1);
				$valor9=($valor9 * 2);
				if($valor9>9){ 
					$valor9=($valor9 - 9); 
				}
				$suma_impares=($valor1 + $valor3 + $valor5 + $valor7 + $valor9);
				$suma=($suma_pares + $suma_impares);
				$dis=substr($suma, 0,1);//extraigo el primer numero de la suma
				$dis=(($dis + 1)* 10);//luego ese numero lo multiplico x 10, consiguiendo asi la decena inmediata superior
				$digito=($dis - $suma);
				if($digito==10){ 
					$digito='0'; 
				}
				if ($digito==$ult_digito){//comparo los digitos final y ultimo
					return TRUE;
				}else{
					$this->form_validation->set_message('option', 'valor de {field} incorrecto');
					return FALSE;
				}
			}
		)));
        $this->form_validation->set_rules('apellidos', 'Apellidos', 'trim|required|max_length[180]');
        $this->form_validation->set_rules('nombres', 'Nombres', 'trim|required|max_length[180]');
        $this->form_validation->set_rules('direccion', 'Direccion', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[200]|is_unique[perfil.email]');
        $this->form_validation->set_rules('genero', 'Genero', 'trim|required');
        $this->form_validation->set_rules('telefono', 'Telefono', 'trim|required|integer|max_length[13]');
        $this->form_validation->set_rules('edad', 'Edad', 'trim|required|integer');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[perfil.username]');
        $this->form_validation->set_rules('password', 'Contraseña', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('password2', 'Confirmacion Contraseña', 'trim|required|matches[password]');
		if (isset($_FILES['img_perfil'])){
			if (empty($_FILES['img_perfil'])){
				$this->form_validation->set_rules('img_perfil', 'Imagen', array('trim', array(
					'option', function($opt){
						$image = getimagesize($GLOBALS['image']['tmp_name']);
						if ($image[0] > $GLOBALS['image_width']){
							$this->form_validation->set_message('option', 'Ancho de {field} excedido');
							return FALSE;
						}else if ($image[1] > $GLOBALS['image_height']){
							$this->form_validation->set_message('option', 'Alto de {field} excedido');
							return FALSE;
						}else if ($GLOBALS['image']['size'] > $GLOBALS['image_size']){
							$this->form_validation->set_message('option', 'Peso de {field} excedido');
							return FALSE;
						}
						return TRUE;
					}
				)));
			}
		}
        # /Validation
        if (!$this->form_validation->run()){
            echo $this->blade->view()->make("page/sing_up");
        }else{
            $level = null;
            $img = '';
            $file = 'avatar/';
            if ($_FILES['img_perfil']['name']){
                $config = array(
                    'upload_path'   => FCPATH.'/uploads/'.$file,
                    'allowed_types' => "jpg|png|jpeg",
                    'overwrite'     => false,
                    'max_size'      => $GLOBALS['image_size'],
                    'max_width'     => $GLOBALS['image_width'],
                    'max_height'    => $GLOBALS['image_height']
                );
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('img_perfil')){
                    $img = "{$file}{$this->upload->data('file_name')}";
                }
            }
            $data = array(
                'cedula' => $this->input->post('cedula', TRUE),
                'apellidos' => $this->input->post('apellidos', TRUE),
                'nombres' => $this->input->post('nombres', TRUE),
                'direccion' => $this->input->post('direccion', TRUE),
                'email' => $this->input->post('email', TRUE),
                'genero' => $this->input->post('genero', TRUE),
                'telefono' => $this->input->post('telefono', TRUE),
                'edad' => $this->input->post('edad', TRUE),
                'estado' => '1',
                'username' => $this->input->post('username', TRUE),
                'password' => $this->input->post('password', TRUE),
                'img_perfil' => $img,
                'informacion' => '',
                'tipo' => 'cliente'
            );
            $this->load->model('modelos');
            $this->modelos->perfil($data);
			redirect('admin/inicio');
		}
	}
}