<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Migration_Tabla_contenido extends CI_Migration {

        /**
         * CREATE TABLE pension IN DATABASE
         * @param null
         */
        public function up(){
            $this->dbforge->add_field(array(
                'id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE),
                'titulo' => array('type' => 'VARCHAR', 'constraint' => 80),
                'descripcion' => array('type' => 'TEXT'),
                'img' => array('type' => 'TEXT'),
                'updated_at' => array('type' => 'TIMESTAMP'),
                'created_at' => array('type' => 'TIMESTAMP'),
            ));
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('contenido', TRUE, ['ENGINE' => 'InnoDB']);
        }

        /**
         * DROP DATABASE pension IN DATABASE
         * @param null
         */
        public function down(){
            $this->dbforge->drop_table('contenido');
        }
    }