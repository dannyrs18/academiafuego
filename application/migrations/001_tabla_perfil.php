<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Migration_Tabla_perfil extends CI_Migration {

        /**
         * 
         */
        public function create_user(){
            $root = array(
                'cedula'     => '1950052934',
                'apellidos'  => 'Sarango',
                'nombres'    => 'Cristian',
                'direccion'  => 'La Banda Alta',
                'email'      => 'mata.sran@gmail.com',
                'genero'     => '1',
                'telefono'   => '0960831063',
                'edad'       => 22,
                'estado'     => 1,
                'username'   => 'cristians',
                'password'   => 'osito21',
                'tipo'       => 'administrador',
                'estado'     => '1',
                'img_perfil' => null,
                'informacion'=> '',
            );
            $this->load->model('modelos');
            $this->modelos->perfil($root);
        }

        /**
         * CREATE TABLE perfil IN DATABASE
         * @param null
         */
        public function up(){

            $this->dbforge->add_field(array(
                'id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE),
                'cedula' => array('type' => 'VARCHAR', 'constraint' => 12, 'unique' => TRUE),
                'apellidos' => array('type' => 'VARCHAR', 'constraint' => 180),
                'nombres' => array('type' => 'VARCHAR', 'constraint' => 180),
                'informacion' => array('type' => 'TEXT'),
                'direccion' => array('type' => 'TEXT'),
                'email' => array('type' => 'VARCHAR', 'constraint' => 200, 'unique'=> TRUE),
                'genero' => array('type' => 'ENUM("0", "1")'),
                'telefono' => array('type' => 'VARCHAR', 'constraint' => 13),
                'edad' => array('type' => 'INT', 'constraint' => 11),
                'estado' => array('type' => 'ENUM("0","1")'),
                'img_perfil' => array('type' => 'TEXT', 'null' => TRUE),
                'username' => array('type' => 'VARCHAR', 'constraint' => 20, 'unique' => TRUE),
                'password' => array('type' => 'TEXT'),
                'tipo' => array('type' => 'ENUM("administrador", "usuario", "coreografo", "cliente")'),
                'level_to' => array('type' => 'ENUM("1", "2", "3", "4")'), // 1->ADMINISTRADOR, 2->USUARIO, 3->COREOGRAFO, 4->CLIENTE
                'slug' => array('type' => 'VARCHAR', 'constraint' => 80, 'unique'=> TRUE),
                'updated_at' => array('type' => 'TIMESTAMP'),
                'created_at' => array('type' => 'TIMESTAMP'),
            ));
            $this->dbforge->add_key('id', TRUE);  // TRUE -> PRIMARY KEY
            $this->dbforge->create_table('perfil', TRUE, ['ENGINE' => 'InnoDB']);
            $this->create_user();
        }

        /**
         * DROP TABLE perfil IN DATABASE
         * @param null
         */
        public function down(){
            $this->dbforge->drop_table('perfil');
        }
    }