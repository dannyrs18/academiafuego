<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Migration_Tabla_inscripcion extends CI_Migration {

        /**
         * CREATE TABLE pre_inscripcion IN DATABASE
         * @param null
         */
        public function up(){
            $this->dbforge->add_field(array(
                    'id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE),
                    'fecha_inscripcion' => array('type' => 'DATE'),
                    'estado' => array('type' => 'ENUM("0","1")'),
                    'slug' => array('type' => 'VARCHAR', 'constraint' => 80, 'unique'=> TRUE),
                    'horario_id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE),
                    'cliente_id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE),
                    'updated_at' => array('type' => 'TIMESTAMP'),
                    'created_at' => array('type' => 'TIMESTAMP'),
                )
            );
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_field("CONSTRAINT FOREIGN KEY (horario_id) REFERENCES horario(id)");
            $this->dbforge->add_field("CONSTRAINT FOREIGN KEY (cliente_id) REFERENCES perfil(id)");
            $this->dbforge->create_table('inscripcion', TRUE, ['ENGINE' => 'InnoDB']);
        }

        /**
         * DROP TABLE pre_inscripcion IN DATABASE
         * @param null
         */
        public function down(){
            $this->dbforge->drop_table('inscripcion');
        }
    }