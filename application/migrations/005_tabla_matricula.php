<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Migration_Tabla_matricula extends CI_Migration {

        /**
         * CREATE TABLE matricula IN DATABASE
         * @param null
         */
        public function up(){
            $this->dbforge->add_field(array(
                'id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE),
                'fecha_matricula' => array('type' => 'DATE'),
                'estado' => array('type' => 'ENUM("0","1")'),
                'slug' => array('type' => 'VARCHAR', 'constraint' => 80, 'unique'=> TRUE),
                'inscripcion_id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE),
                'updated_at' => array('type' => 'TIMESTAMP'),
                'created_at' => array('type' => 'TIMESTAMP'),
            ));
            $this->dbforge->add_key(['id', 'inscripcion_id'], TRUE);
            $this->dbforge->add_field("CONSTRAINT FOREIGN KEY (inscripcion_id) REFERENCES inscripcion(id)");
            $this->dbforge->create_table('matricula', TRUE, ['ENGINE' => 'InnoDB']);
        }
        
        /**
         * DROP DATABASE matricula IN DATABASE
         * @param null
         */
        public function down(){
            $this->dbforge->drop_table('matricula');
        }
    }