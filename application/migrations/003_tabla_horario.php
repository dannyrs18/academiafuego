<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Migration_Tabla_horario extends CI_Migration {

        /**
         * CREATE TABLE horario IN DATABASE
         * @param null
         */
        public function up(){
            $this->dbforge->add_field(array(
                'id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE),
                'tipo_curso' => array('type' => 'ENUM("Permanente","Temporal","Vacacional")'),
                'fecha_inicio' => array('type' => 'DATE'),
                'costo' => array('type' => 'FLOAT', 'constraint' => 11),
                'cupo' => array('type' => 'INT', 'constraint' => 3),
                'fecha_fin' => array('type' => 'DATE'),
                'duracion' => array('type' => 'ENUM("2","4","10")'),
                'hora_inicio' => array('type' => 'TIME'),
                'hora_fin' => array('type' => 'TIME'),
                'estado' => array('type' => 'ENUM("0", "1")'),
                'slug' => array('type' => 'VARCHAR', 'constraint' => 80, 'unique'=> TRUE),
                'curso_id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE),
                'coreografo_id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE),
                'updated_at' => array('type' => 'TIMESTAMP'),
                'created_at' => array('type' => 'TIMESTAMP'),
            ));
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_field("CONSTRAINT FOREIGN KEY (curso_id) REFERENCES curso(id) ON DELETE CASCADE");
            $this->dbforge->add_field("CONSTRAINT FOREIGN KEY (coreografo_id) REFERENCES perfil(id) ON DELETE CASCADE");
            $this->dbforge->create_table('horario', TRUE, ['ENGINE' => 'InnoDB']);
        }

        /**
         * DROP TABLE horario IN DATABASE
         * @param null
         */
        public function down(){
            $this->dbforge->drop_table('horario');
        }
    }