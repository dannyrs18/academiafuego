<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Migration_Tabla_curso extends CI_Migration {

        /**
         * CREATE TABLE curso IN DATABASE
         * @param null
         */
        public function up(){
            $this->dbforge->add_field(array(
                'id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE),
                'disciplina' => array('type' => 'VARCHAR', 'constraint' => 60),
                'descripcion' => array('type' => 'TEXT'),
                'img_curso' => array('type' => 'TEXT', 'null' => TRUE),
                'estado' => array('type' => 'ENUM("0", "1")'),
                'slug' => array('type' => 'VARCHAR', 'constraint' => 80, 'unique'=> TRUE),
                'updated_at' => array('type' => 'TIMESTAMP'),
                'created_at' => array('type' => 'TIMESTAMP'),
            ));
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('curso', TRUE, ['ENGINE' => 'InnoDB']);
        }

        /**
         * DROP TABLE curso IN DATABASE
         * @param null
         */
        public function down(){
            $this->dbforge->drop_table('curso');
        }
    }