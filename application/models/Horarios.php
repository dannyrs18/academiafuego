<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Horarios extends Eloquent {

    protected $table = "horario"; // table name

    public function coreografo(){
        return Perfiles::find($this->coreografo_id);
    }

    public function curso(){
        return Cursos::find($this->curso_id);
    }

    public function inscripciones(){
        return Inscripciones::all()->where('horario_id', $this->id);
    }
 
}

# Execute composer dump-autoload