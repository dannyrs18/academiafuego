<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Perfiles extends Eloquent {

    protected $table = "perfil"; // table name

    /**
     * @method
     * @param
     */
    public function inscripciones(){
        return Inscripciones::all()->where('cliente_id', $this->id);
    }

    public function get_simple_name(){
        return explode(' ',$this->nombres)[0].' '.explode(' ', $this->apellidos)[0];
    }

    public function get_full_name(){
        return $this->nombres.' '.$this->apellidos;
    }

}

# Execute composer dump-autoload