<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Inscripciones extends Eloquent {

    protected $table = "inscripcion"; // table name


    /**
     * @method
     * @param
     */
    public function cliente(){
        return Perfiles::find($this->cliente_id);
    }

    /**
     * @method
     * @param
     */
    public function horario(){
        return Horarios::find($this->horario_id);
    }

    /**
     * @method
     * @param
     */
    public function matricula(){
        return Matriculas::find($this->id);
    }

}

# Execute composer dump-autoload